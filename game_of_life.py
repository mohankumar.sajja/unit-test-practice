def next_state(current_state, alive_neighbors):
    if (current_state and alive_neighbors is 2) or alive_neighbors is 3:
        return True
    return False

def evolution(grid, row, col):
    num_rows = len(grid)
    num_cols = len(grid[0])
    count = 0

    if row in range(num_rows) and col in range(num_cols):
        if row == 0:
            lower_row = 0
            upper_row = 1
        elif row == num_rows - 1:
            lower_row = row - 1
            upper_row = row
        else:
            lower_row = row-1
            upper_row = row+1

        if col == 0:
            lower_col = 0
            upper_col = 1
        elif col == num_cols - 1:
            lower_col = col - 1
            upper_col = col
        else:
            lower_col = col - 1
            upper_col = col + 1


    for x in range(lower_row, upper_row + 1):
        for y in range(lower_col, upper_col + 1):
            if x == row and y == col:
                continue
            else:
                if grid[x][y] == True:
                    count += 1

    return next_state(grid[row][col], count)
    