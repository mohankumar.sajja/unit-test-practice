import unittest
from game_of_life import *

DEAD = False
ALIVE = True

class TestGameOfLife(unittest.TestCase):

    def test_underpopulation(self):
        expected_output = DEAD
        current_state = ALIVE
        alive_neighbors = 0
        output = next_state(current_state, alive_neighbors)
        self.assertEqual(output, expected_output)

        alive_neighbors = 1
        output = next_state(current_state, alive_neighbors)
        self.assertEqual(output, expected_output)

    def test_state_normal(self):
        expected_output = ALIVE
        current_state = ALIVE
        alive_neighbors = 2
        output = next_state(current_state, alive_neighbors)
        self.assertEqual(output, expected_output)

        alive_neighbors = 3
        output = next_state(current_state, alive_neighbors)
        self.assertEqual(output, expected_output)

    def test_overpopulation(self):
        expected_output = DEAD
        current_state = ALIVE
        alive_neighbors = 4
        output = next_state(current_state, alive_neighbors)
        self.assertEqual(output, expected_output)

        alive_neighbors = 5
        output = next_state(current_state, alive_neighbors)
        self.assertEqual(output, expected_output)

        alive_neighbors = 6
        output = next_state(current_state, alive_neighbors)
        self.assertEqual(output, expected_output)
        
        alive_neighbors = 7
        output = next_state(current_state, alive_neighbors)
        self.assertEqual(output, expected_output)
        
        alive_neighbors = 8
        output = next_state(current_state, alive_neighbors)
        self.assertEqual(output, expected_output)

    def test_reproduction(self):
        expected_output = ALIVE
        current_state = DEAD
        alive_neighbors = 3
        output = next_state(current_state, alive_neighbors)
        self.assertEqual(output, expected_output)



        expected_output = DEAD
        current_state = DEAD
        alive_neighbors = 0
        output = next_state(current_state, alive_neighbors)
        self.assertEqual(output, expected_output)

        alive_neighbors = 1
        output = next_state(current_state, alive_neighbors)
        self.assertEqual(output, expected_output)

        alive_neighbors = 2
        output = next_state(current_state, alive_neighbors)
        self.assertEqual(output, expected_output)

        alive_neighbors = 4
        output = next_state(current_state, alive_neighbors)
        self.assertEqual(output, expected_output)

        alive_neighbors = 5
        output = next_state(current_state, alive_neighbors)
        self.assertEqual(output, expected_output)

        alive_neighbors = 6
        output = next_state(current_state, alive_neighbors)
        self.assertEqual(output, expected_output)

        alive_neighbors = 7
        output = next_state(current_state, alive_neighbors)
        self.assertEqual(output, expected_output)

        alive_neighbors = 8
        output = next_state(current_state, alive_neighbors)
        self.assertEqual(output, expected_output)

    def test_evolution(self):
        # Test for Under Population
        x = 0
        y = 0
        grid = [[ALIVE, DEAD, DEAD],[DEAD, DEAD, DEAD],[DEAD, DEAD, DEAD]] 
        output = evolution(grid, x, y)
        expected_output = DEAD
        self.assertEqual(output, expected_output)

        # Test for Re-production
        x = 0
        y = 1
        grid = [[ALIVE, DEAD, ALIVE],[ALIVE, DEAD, DEAD],[DEAD, DEAD, DEAD]] 
        output = evolution(grid, x, y)
        expected_output = ALIVE
        self.assertEqual(output, expected_output)

        # Test for Over Population
        x = 0
        y = 1
        grid = [[ALIVE, ALIVE, ALIVE],[ALIVE, ALIVE, DEAD],[DEAD, DEAD, DEAD]] 
        output = evolution(grid, x, y)
        expected_output = DEAD
        self.assertEqual(output, expected_output)

        # Test for Normal State / Next Generation
        x = 0
        y = 1
        grid = [[ALIVE, ALIVE, ALIVE],[ALIVE, DEAD, DEAD],[DEAD, DEAD, DEAD]] 
        output = evolution(grid, x, y)
        expected_output = ALIVE
        self.assertEqual(output, expected_output)

        x = 0
        y = 1
        grid = [[DEAD, ALIVE, ALIVE],[ALIVE, DEAD, DEAD],[DEAD, DEAD, DEAD]] 
        output = evolution(grid, x, y)
        expected_output = ALIVE
        self.assertEqual(output, expected_output)
        
        # Test for Edge Cases
        x = 2
        y = 2
        grid = [[DEAD, ALIVE, ALIVE],[ALIVE, ALIVE, ALIVE],[DEAD, ALIVE, DEAD]] 
        output = evolution(grid, x, y)
        expected_output = ALIVE
        self.assertEqual(output, expected_output)

        x = 1
        y = 1
        grid = [[DEAD, ALIVE, ALIVE],[ALIVE, ALIVE, ALIVE],[DEAD, ALIVE, DEAD]] 
        output = evolution(grid, x, y)
        expected_output = DEAD
        self.assertEqual(output, expected_output)

        x = 2
        y = 1
        grid = [[DEAD, ALIVE, ALIVE],[ALIVE, ALIVE, ALIVE],[DEAD, ALIVE, DEAD]] 
        output = evolution(grid, x, y)
        expected_output = ALIVE
        self.assertEqual(output, expected_output)